# Python image.
# - https://docs.docker.com/language/python/build-images/
FROM python:3.11-slim-buster

WORKDIR /app

# Set timezone.
# - https://dev.to/0xbf/set-timezone-in-your-docker-image-d22
# Official Debian and Ubuntu images automatically run apt-get clean, so explicit invocation is not required.
# - https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#run
RUN apt update && apt install tzdata -y
ENV TZ="Europe/Berlin"

COPY requirements.txt .
RUN pip3 install --no-cache-dir -r requirements.txt

COPY src/ .
RUN chmod +x langfingaz/main.py

# pythonpath is required to import from self created modules ("from langfingaz ...")
ENV PYTHONPATH=/app/
# unbuffered output, otherwise script sleeps before any output appears
ENV PYTHONUNBUFFERED=1

ENTRYPOINT [ "python3", "./langfingaz/main.py" ]
CMD [ "log-verbose" ]
