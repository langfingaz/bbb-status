# bbb-status

Python3 script to log and plot BigBlueButton usage statistics.

Thanks to Bernd Wurst who inspired this project with his gist on github:
* [https://gist.github.com/bwurst/7f94e0392c75d273a08d1e686182fc5e]()

## Setup

Execute `bbb-conf --secret` on your server.

This gives you your **API-secret** and the **URL** at which BigBlueButton is running at.

Save these two values in the first line of the following files:

* URL inside `secret/url.txt`
* API-secret inside `secret/secret.txt`

Note: You may `chmod 600` on the two above files.

Optionally: Set the log interval to something different from 5 minutes by creating `secret/minutes.txt` with an integer in the first line.

## Run

Start logging in the background:

```shell
docker-compose up -d
```

Generate a new plot based on saved meeting data:

```shell
sudo docker-compose run bbb-status plot
```

## Example - log meeting statistics

```shell
sudo docker-compose up -d  # start logging in the background
sudo docker-compose logs  # inspect container output
```
```shell
Attaching to bbb-status_bbb-status_1
bbb-status_1  | === bbb-status ===
bbb-status_1  | 
bbb-status_1  | BBB meetingData logger started!
bbb-status_1  | Saved meetings at /usr/data/20210217_173525_meetings.xml
bbb-status_1  | 	meetings: [<langfingaz.parseMeetings.Meeting object at 0x7f8382471280>]
bbb-status_1  | 	pointOfTime: None
bbb-status_1  | 	recordingCount: 1
bbb-status_1  | 	participantCount: 4
bbb-status_1  | 	listenerCount: 0
bbb-status_1  | 	voiceParticipantCount: 4
bbb-status_1  | 	videoCount: 4
bbb-status_1  | 	moderatorCount: 1
bbb-status_1  | >> Sleeping for five minutes <<
```

## Example - create a plot from saved statistics

![plot of one month BBB usage](plot-last-month.png)
