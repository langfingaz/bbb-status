#!/usr/bin/python3
from sys import argv
import logging

from langfingaz import plotMeetings
from langfingaz import logMeetingData
from langfingaz import recentModerators


def main():
    # logging.basicConfig(level=logging.DEBUG)

    print("=== bbb-status ===")
    logging.debug(str(argv))
    print()

    usageStr = 'Usage:\n' + argv[0] + ' [log|log-verbose|plot]\n' + \
               '\tlog: log BBB meeting data every 5 minutes;\n' + \
               '\t  prints one dot after each successfully saved file\n' + \
               '\tlog-verbose: log BBB meeting every 5 minute and write current status to stdout\n' + \
               '\tplot: saves a plot of the saved BBB meeting data\n' + \
               '\tmoderator: prints users that have recently been moderators in meetings\n'

    if len(argv) != 2:
        raise ValueError("Expected one commandline argument!\n" + usageStr)

    if argv[1] == "log":
        logMeetingData.v1()
    elif argv[1] == "log-verbose":
        logMeetingData.v2()
    elif argv[1] == "plot":
        plotMeetings.plotMeetings()
    elif argv[1] == "moderator":
        recentModerators.printModerators()
    else:
        print(usageStr)
        exit(1)


if __name__ == '__main__':
    main()
