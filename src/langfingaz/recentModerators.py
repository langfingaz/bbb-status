from datetime import datetime, timedelta
from pathlib import Path
from typing import List, Dict
import logging

from langfingaz import parseMeetings
from langfingaz.parseMeetings import BbbStatus, Meeting
from langfingaz.util import fileUtil
from langfingaz.util import util


def getModerators(dataDir: Path = fileUtil.getDataDir()) -> Dict[parseMeetings.User, int]:
    """
    Logged in users have a consistent userId and fullName.
    But if they join a meeting via a link they might be asked for a name and get a new userId.

    This method goes through all BbbStati and filters for moderators.
    It then counts how often a moderator was found in one BbbStati.

    If a BbbStati are saved every 5 minutes, a user might have a count of 4 if he was moderator
    at just one meeting for >20 minutes

    :return: Dict[User, count-how-often-as-moderator] sorted by User.fullName
    """

    bbbStati: List[BbbStatus] = parseMeetings.readBbbStatiFromDir(dataDir=dataDir, delta=timedelta(days=28))
    if len(bbbStati) < 1:
        return {}

    moderators: List[parseMeetings.User] = [attendee.user
                                            for bbbStatus in bbbStati
                                            for meeting in bbbStatus.meetings
                                            for attendee in meeting.attendees
                                            if attendee.role == "MODERATOR"]

    moderatorCount: Dict[parseMeetings.User, int] = {}
    moderator: parseMeetings.User
    for moderator in moderators:
        count: int = 1
        if moderator in moderatorCount:
            count = moderatorCount.get(moderator) + 1
        moderatorCount[moderator] = count

    # sort by User.fullName
    moderatorCount = dict(sorted(moderatorCount.items(), key=lambda item: item[0].fullName))
    return moderatorCount


def printModerators():
    """
    There may be multiple (different) User objects (with different UserIds) that have the
    same name. This may come from one user joining different meetings.

    This method reduces Users to their fullNames.
    """

    moderatorUsers: Dict[parseMeetings.User, int] = getModerators()

    moderatorNames: Dict[str, int] = {}
    user: parseMeetings.User
    for user, count in moderatorUsers.items():
        if user.fullName in moderatorNames:
            count += moderatorNames[user.fullName]
        moderatorNames[user.fullName] = count

    [print(fullName + ":\t" + str(count)) for fullName, count in moderatorNames.items()]
