from datetime import datetime
from pathlib import Path

import langfingaz.util.fileUtil as fileUtil
from langfingaz import bbbRequest


def requestAndSaveMeetingData(folder: Path = fileUtil.getDataDir()) -> Path:
    """
    save a new xml file in the given folder

    :param dataStr:
    :param folder:
    :return: Path to created file
    """

    return saveMeetingsData(bbbRequest.requestMeetingData(), folder)


def saveMeetingsData(dataStr: str, folder: Path = fileUtil.getDataDir()) -> Path:
    """
    save a new xml file in the given folder

    :param dataStr:
    :param folder:
    :return: Path to created file
    """

    return doSaveData(dataStr, folder, 'meetings')


def doSaveData(dataStr: str, folder: Path, dataType: str) -> Path:
    """
    save a new xml file in the given folder

    :param dataStr:
    :param folder:
    :param dataType: e.g. "meetings" for meeting XML data
    :return: Path to created file
    """

    fileWithoutDate = folder / f"{dataType}.xml"
    prefixedFile = fileUtil.setDatetimePrefix(fileWithoutDate, datetime.now())

    with open(prefixedFile, "w") as xml_file:
        xml_file.write(dataStr)
    return prefixedFile
