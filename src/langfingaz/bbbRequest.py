import hashlib
import logging

import requests
from requests.adapters import HTTPAdapter
import requests.packages.urllib3.exceptions
from requests.packages.urllib3.util.retry import Retry

from xml.etree import ElementTree

import langfingaz.util.fileUtil as fileUtil


def requestMeetingData() -> str:
    """
    :return: XML file containing data about currently active BBB meetings
      See also: https://docs.bigbluebutton.org/dev/api.html#monitoring
      as well as: https://docs.bigbluebutton.org/dev/api.html#getmeetings
    """

    requestUrl = getRequestUrl()
    response: requests.Response = doGetRequest(requestUrl)

    tree = ElementTree.fromstring(response.content)
    if tree.find('returncode').text != 'SUCCESS':
        raise ValueError('error getting API data')

    return str(response.content, encoding=response.encoding)


def doGetRequest(requestUrl: str, maxRetries=6) -> requests.Response:
    """
    Performs a HTTPS GET request for the given url and does at most maxRetries retries
    on connection errors.

    :raises requests.exceptions.HTTPError: If status code not 200 (will retry if 502, 503, 504)
    :raises requests.packages.urllib3.exceptions.MaxRetryError:
    """

    logLevel = logging.root.level  # https://stackoverflow.com/a/53951759/6334421
    logging.basicConfig(level=logging.DEBUG)

    # https://stackoverflow.com/a/35636367/6334421
    # Retry on basic connectivity issues (including DNS lookup failures),
    # and HTTP status codes of 502, 503 and 504
    # Grace period increases after each retry.
    # See also:
    # - Session: https://requests.readthedocs.io/en/master/user/advanced/
    s: requests.sessions.Session = requests.Session()
    retries: Retry = Retry(total=maxRetries, backoff_factor=5, status_forcelist=[502, 503, 504])
    s.mount('https://', HTTPAdapter(max_retries=retries))

    response: requests.Response = s.get(requestUrl)
    logging.basicConfig(level=logLevel)

    # raise exception if status code NOT 200
    response.raise_for_status()

    return response


def getRequestUrl(api_method: str = 'getMeetings', query_string: str = '') -> str:
    url = getUrl()
    api_url = url + "api/"
    secret = getSecret()
    h = hashlib.sha1((api_method + query_string + secret).encode('utf-8'))
    checksum = h.hexdigest()

    if len(query_string) > 0:
        query_string += '&'

    return api_url + api_method + '?' + query_string + 'checksum=' + checksum


def getUrl() -> str:
    filepath = fileUtil.getProjectBaseDir() / "secret" / "url.txt"
    url = fileUtil.readFirstLine(filepath).strip()
    if not url.endswith("/"):
        raise ValueError("url should end with '/'")
    return url


def getSecret() -> str:
    filepath = fileUtil.getProjectBaseDir() / "secret" / "secret.txt"
    secret = fileUtil.readFirstLine(filepath).strip()
    min_length = 12
    if len(secret) <= min_length:
        raise ValueError(f"secret should be longer than {min_length} characters!")
    return secret


def getSleepMinutes() -> int:
    filepath = fileUtil.getProjectBaseDir() / "secret" / "minutes.txt"
    if not filepath.exists():
        return 5
    return int(fileUtil.readFirstLine(filepath).strip())
