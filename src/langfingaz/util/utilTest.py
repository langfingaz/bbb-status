import unittest
from langfingaz.util import util as util
import langfingaz.util.fileUtil as fileUtil
from pathlib import Path
from datetime import datetime


class UtilTestCase(unittest.TestCase):
    def test_indentString(self):
        unindented = "Hello\nWorld!"
        expected = "\tHello\n\tWorld!"

        actual = util.indentMultilineStr(unindented)

        self.assertEqual(expected, actual)

    def test_indentString2(self):
        unindented = "Hello\nWorld!\n"
        expected = "\tHello\n\tWorld!\n"

        actual = util.indentMultilineStr(unindented)

        self.assertEqual(expected, actual)

    def test_addPrefixDate(self):
        file: Path = Path("/foo/bar")
        t: datetime = datetime.strptime('20200101_120030', '%Y%m%d_%H%M%S')
        expectedFile: Path = Path("/foo/20200101_120030_bar")

        # ACT
        prefixedFile: Path = fileUtil.setDatetimePrefix(file, t)

        # ASSERT
        self.assertEqual(type(expectedFile), type(prefixedFile))
        self.assertEqual(expectedFile, prefixedFile)

    def test_readPrefixDate(self):
        file: Path = Path("/foo/20200101_120030_bar")
        expectedDate = datetime.replace(year=2020, month=1, day=1, hour=12, minute=0, second=1)

        # ACT
        dateFromFile: datetime = fileUtil.getDatetimePrefix(file)

        # ASSERT
        self.assertEqual(type(expectedDate), type(dateFromFile))
        self.assertEqual(expectedDate, dateFromFile)

    def test_setAndRemoveDatetimePrefix(self):
        t: datetime = datetime.strptime('20200101_120030', '%Y%m%d_%H%M%S')

        file: Path = Path("/foo/bar")
        prefixed: Path = fileUtil.setDatetimePrefix(file, t)
        actual: Path = fileUtil.removeDatetimePrefix(prefixed)

        self.assertEqual(type(file), type(actual))
        self.assertEqual(file, actual)


if __name__ == '__main__':
    unittest.main()
