import time
import datetime


def sleep(seconds: float):
    print(flush=True)  # print newline -> so that all characters of current line get flushed as well
    time.sleep(seconds)


def indentMultilineStr(s: str, indentWith='\t'):
    indented = indentWith + s.replace('\n', '\n' + indentWith)
    if s.endswith('\n'):
        indented = indented[:len(indented) - len(indentWith)]
    return indented


# def roundFloorByDay(t) -> datetime.date:
#     """
#     rounds the (date)time up to the closest full day before or equal this point of time
#     (round floor)
#
#     :param t: datetime.date or datetime.datetime
#     :return: the closest full day before or equal to (date)time t
#     """
#
#     # note: order of datetime and date type check matters
#     #  as datetime is a SUBCLASS of date
#     if issubclass(type(t), datetime.datetime):
#         return t.date()
#     elif issubclass(type(t), datetime.date):
#         return t
#     else:
#         raise ValueError("illegal argument")


def roundCeilingByDay(t) -> datetime.date:
    """
    rounds the (date)time up to the closest full day after or equal to this point of time
    (round ceiling)

    :param t: datetime.date or datetime.datetime
    :return: the closest full day after or equal to (date)time t
    """

    # note: order of datetime and date type check matters
    #  as datetime is a SUBCLASS of date
    if issubclass(type(t), datetime.datetime):
        t: datetime.datetime  # "cast" to datetime.datetime for IDE linting

        if t.minute == 0 and t.second == 0 and t.microsecond == 0:
            return t.date()
        else:
            return datetime.date(year=t.year, month=t.month, day=t.day + 1)
    elif issubclass(type(t), datetime.date):
        return t
    else:
        raise ValueError("illegal argument")


def asString(o: object):
    attrs = vars(o)  # attributes and their values
    return '\n'.join("%s: %s" % item for item in attrs.items())


def asBoolean(booleanValue: str):
    """
    :param booleanValue: Some boolean value in string representation
    :return: the boolean value as boolean object
    :raise ValueError: If booleanValue could not be parsed as boolean object
    """
    if booleanValue.lower() in ['true', 'yes']:
        return True
    elif booleanValue.lower() in ['false', 'no']:
        return False
    else:
        raise ValueError("booleanValue (string) could not be parsed to boolean object!")

# def listAsString(l: list):
#     x = ["(", ")"]
#     return ", ".join(l).join(x)
