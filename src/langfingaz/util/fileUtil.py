from datetime import datetime
from pathlib import Path
import os


def getWorkingDir() -> Path:
    return Path(os.path.dirname(os.path.realpath(__file__)))


def getProjectBaseDir() -> Path:
    return getWorkingDir().parent.parent.parent


def getDataDir() -> Path:
    """
    :return: The default data directory
    """
    return getProjectBaseDir() / "data"


def readFirstLine(file: Path) -> str:
    """
    :param file: Path to file
    :return: first line of file
    """
    with open(file, "r") as f:
        return f.readline()


def __getDatetimePrefixLength() -> int:
    return len("20200101_120030")  # 1st January 2020, 12:00 and 30 seconds


def asString(t: datetime) -> str:
    return t.strftime('%Y%m%d_%H%M%S')


def setDatetimePrefix(file: Path, t: datetime) -> Path:
    # filename = file.name
    prefix = asString(t)
    filename = prefix + "_" + file.name
    return file.parent / filename


def removeDatetimePrefix(file: Path) -> Path:
    prefixLen = __getDatetimePrefixLength()

    # prefixlen + 1 to remove the underline!
    return file.parent / file.name[prefixLen + 1:]


def getDatetimePrefix(file: Path) -> datetime:
    """
    :param file: some file which filename is prefixed with a date in the form %Y%m%d_%H%M%S
    :return: date from filename
    """
    prefixLen = __getDatetimePrefixLength()

    if len(file.name) < prefixLen:
        raise ValueError("Given file does not seem to contain a datetime prefix!")
    prefix = file.name[:prefixLen]
    return datetime.strptime(prefix, '%Y%m%d_%H%M%S')
