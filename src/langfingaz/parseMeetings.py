from functools import total_ordering
from typing import List
from pathlib import Path
from xml.etree import ElementTree
from datetime import datetime, timedelta

import logging

from langfingaz import loadData
from langfingaz.util import util
from langfingaz.util import fileUtil


class Meeting(object):
    def __init__(self, xml_meeting: ElementTree.Element):
        self.meetingName: str = xml_meeting.find('meetingName').text
        self.meetingID: str = xml_meeting.find('meetingID').text
        self.internalMeetingID: str = xml_meeting.find('internalMeetingID').text

        self.isRunning: bool = util.asBoolean(xml_meeting.find('running').text)
        self.startTime: int = int(xml_meeting.find('startTime').text)
        self.createTime: int = int(xml_meeting.find('createTime').text)
        if self.isRunning:
            self.endTime = None
        else:
            self.endTime = xml_meeting.find('endTime').text

        # TODO: disable default meeting recordings (!)
        self.isRecording: bool = util.asBoolean(xml_meeting.find('recording').text)
        self.isBreakout: bool = util.asBoolean(xml_meeting.find('isBreakout').text)

        self.participantCount: int = int(xml_meeting.find('participantCount').text)
        self.listenerCount: int = int(xml_meeting.find('listenerCount').text)
        self.voiceParticipantCount: int = int(xml_meeting.find('voiceParticipantCount').text)
        self.videoCount: int = int(xml_meeting.find('videoCount').text)
        self.maxUsers: int = int(xml_meeting.find('maxUsers').text)
        self.moderatorCount: int = int(xml_meeting.find('moderatorCount').text)

        self.attendees: List[Attendee] = [Attendee(xml_attendee) for xml_attendee in
                                          xml_meeting.find('attendees').iter('attendee')]

    def __str__(self):
        return util.asString(self)


class Attendee(object):
    def __init__(self, xml_attendee: ElementTree.Element):
        userID = xml_attendee.find('userID').text
        fullName = xml_attendee.find('fullName').text
        self.user = User(userID=userID, fullName=fullName)

        # roles: MODERATOR, VIEWER
        self.role = xml_attendee.find('role').text
        self.isPresenter: bool = util.asBoolean(xml_attendee.find('isPresenter').text)
        self.isListeningOnly: bool = util.asBoolean(xml_attendee.find('isListeningOnly').text)
        self.hasJoinedVoice: bool = util.asBoolean(xml_attendee.find('hasJoinedVoice').text)
        self.hasVideo: bool = util.asBoolean(xml_attendee.find('hasVideo').text)

    def __str__(self):
        return util.asString(self)


@total_ordering
class User(object):
    def __init__(self, userID: str, fullName: str):
        self.userID = userID
        self.fullName = fullName

    @staticmethod
    def _is_valid_operand(other):
        return (hasattr(other, "userID") and
                hasattr(other, "fullName"))

    def __hash__(self):
        return hash(self.userID)

    def __eq__(self, other):
        if not self._is_valid_operand(other):
            return NotImplemented
        return self.userID == other.userID

    def __lt__(self, other):
        if not self._is_valid_operand(other):
            return NotImplemented
        return self.userID < other.userID

    def __str__(self):
        return self.userID + " - " + self.fullName


class BbbStatus(object):
    """
    Represents the status of one BBB server at one point of time.
    It contains statistics about the running meetings on the server
    and (TODO) it's CPU utilization and network usage.
    """

    pointOfTime: datetime = None

    @staticmethod
    def getKey(bbbStatus):
        """
        Returns the date of this object as key for sorting.
        See also: https://docs.python.org/3.7/howto/sorting.html#key-functions

        :return: Key that can be used for sorting
        """
        return bbbStatus.pointOfTime

    def __init__(self, meetings: List[Meeting], pointOfTime: datetime = None):
        """
        :param meetings: All current meetings
        :param pointOfTime: The date and time at which the information about
          the running meetings has been captured / requested from the BBB instance
        """
        self.meetings = meetings
        self.pointOfTime = pointOfTime

        self.recordingCount = 0
        self.participantCount = 0
        self.listenerCount = 0
        self.voiceParticipantCount = 0
        self.videoCount = 0
        self.moderatorCount = 0

        for meeting in meetings:
            self.participantCount += meeting.participantCount
            self.listenerCount += meeting.listenerCount
            self.voiceParticipantCount += meeting.voiceParticipantCount
            self.videoCount += meeting.videoCount
            self.moderatorCount += meeting.moderatorCount
            if meeting.isRecording:
                self.recordingCount += 1

    def __str__(self):
        return util.asString(self)


def parseMeetingsData(dataStr: str) -> List[Meeting]:
    tree = ElementTree.fromstring(dataStr)
    xml_meetings = tree.find('meetings')

    return [Meeting(xml_meeting) for xml_meeting in xml_meetings.iter('meeting')]


def readBbbStatiFromDir(dataDir: Path = fileUtil.getDataDir(), delta: timedelta = None) -> List[BbbStatus]:
    """
    :arg delta: (optional) Only return BbbStatus objects in the time-range now-delta and now. Example: timedelta(days=28)
    :return: List of BbbStatus objects, sorted by date
    """

    bbbStati: List[BbbStatus] = []

    for file in dataDir.iterdir():
        if file.name.endswith(".xml"):
            logging.debug("Reading from file " + str(file))
            dataStr, t = loadData.loadData(file)
            meetings: List[Meeting] = parseMeetingsData(dataStr)
            bbbStati.append(BbbStatus(meetings, t))
    if (len(bbbStati) < 1):
        print("No bbbStatus objects were found in data directory: " + str(dataDir))
        return []

    # sort by date
    bbbStati = sorted(bbbStati, key=BbbStatus.getKey)

    if delta is not None and len(bbbStati) > 1:
        # filter: take only bbbStatus objects of the last month (4 weeks)
        #
        # start: datetime = bbbStati[0].pointOfTime
        # end: datetime = bbbStati[-1].pointOfTime
        # delta: timedelta = end-start
        end: datetime = bbbStati[-1].pointOfTime
        monthBeforeEnd: datetime = end - delta
        bbbStati = [bbbStatus for bbbStatus in bbbStati if bbbStatus.pointOfTime >= monthBeforeEnd]

    return bbbStati
