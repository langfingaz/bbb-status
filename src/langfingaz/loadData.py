from typing import Tuple
from datetime import datetime
from pathlib import Path

from langfingaz.util import fileUtil


def loadData(file: Path) -> Tuple[str, datetime]:
    dataStr: str = file.read_text()
    t: datetime = fileUtil.getDatetimePrefix(file)

    return dataStr, t
