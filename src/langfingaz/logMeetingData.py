#!/usr/bin/python3
from pathlib import Path

from langfingaz import parseMeetings, bbbRequest, saveData
from langfingaz.bbbRequest import getSleepMinutes
from langfingaz.util import util as util
import langfingaz.util.fileUtil as fileUtil


def sleepMinutes(verbose=False):
    minutes = getSleepMinutes()
    seconds = minutes * 60

    if verbose:
        print(f">> Sleeping for {minutes} minutes <<")
    util.sleep(seconds)


def v2(folder: Path = fileUtil.getDataDir()):
    print("BBB meetingData logger started!")

    while True:
        meetingsStr = bbbRequest.requestMeetingData()
        savedFile = saveData.saveMeetingsData(meetingsStr, folder)
        print(f"Saved meetings at {savedFile}")

        meetings = parseMeetings.parseMeetingsData(meetingsStr)
        bbbStatus = parseMeetings.BbbStatus(meetings)
        bbbStatusStr = str(bbbStatus)
        print(util.indentMultilineStr(bbbStatusStr))

        sleepMinutes(verbose=True)


def v1(folder: Path = fileUtil.getDataDir()):
    while True:
        saveData.requestAndSaveMeetingData(folder)
        print('.')
        sleepMinutes()


if __name__ == '__main__':
    v2()
    exit(1)
