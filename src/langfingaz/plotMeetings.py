from datetime import datetime, timedelta
from pathlib import Path
from typing import List
import logging

import matplotlib.pyplot as plt
import matplotlib.dates as mdates

from langfingaz import loadData
from langfingaz import parseMeetings
from langfingaz.parseMeetings import BbbStatus, Meeting
from langfingaz.util import fileUtil
from langfingaz.util import util


def getDefaultPlotFolder() -> Path:
    return fileUtil.getProjectBaseDir() / "plot"


def plotMeetings(dataDir: Path = fileUtil.getDataDir()):
    """
    plot BBB meetings of the last month
    """

    bbbStati: List[BbbStatus] = parseMeetings.readBbbStatiFromDir(dataDir=dataDir, delta=timedelta(days=28))
    if len(bbbStati) < 1:
        return

    image: Path = doPlotMeetings(bbbStati)
    print("saved image at " + str(image))


def doPlotMeetings(bbbStati: List[BbbStatus]) -> Path:
    '''
    :param bbbStati: List of bbbStatus objects sorted by date
    '''

    time = []  # x-axis: time
    participants = []  # yAxis (1)
    videos = []  # yAxis (2)
    voices = []  # yAxis (3)

    for bbbStatus in bbbStati:
        time.append(bbbStatus.pointOfTime)
        participants.append(bbbStatus.participantCount)
        videos.append(bbbStatus.videoCount)
        voices.append(bbbStatus.voiceParticipantCount)

    _a, _b = plt.subplots()  # Create a figure and an axes.
    fig: plt.Figure = _a
    ax = _b  # of type plt.axes.Axes ??
    if not (issubclass(type(fig), plt.Figure)):
        raise ValueError("expected Figure")

    ax.plot(time, participants, label='participants')
    ax.plot(time, videos, label='video')
    ax.plot(time, voices, label='voice')

    # format the ticks (on x-axis)
    days = mdates.DayLocator(interval=2)  # every second days
    hours = mdates.HourLocator(interval=12)  # every 12 hours one minor tick
    dateFmt = mdates.DateFormatter('%Y-%m-%d')

    ax.xaxis.set_major_locator(days)
    ax.xaxis.set_major_formatter(dateFmt)
    ax.xaxis.set_minor_locator(hours)

    # round to nearest day
    dateMin = time[0].date()  # round floor
    dateMax = util.roundCeilingByDay(time[-1])  # round ceiling
    ax.set_xlim(dateMin, dateMax)

    ax.set_xlabel('time')  # Add an x-label to the axes.
    ax.set_ylabel('numbers')  # Add a y-label to the axes.
    ax.set_title("BigBlueButton Statistics")  # Add a title to the axes.
    ax.legend()  # Add a legend.

    # rotates and right aligns the x labels (dates), and moves the bottom of the
    # axes up to make room for them
    # https://matplotlib.org/api/_as_gen/matplotlib.figure.Figure.html#matplotlib.figure.Figure.autofmt_xdate
    fig.autofmt_xdate()

    imgFormat = 'png'
    image: Path = getDefaultPlotFolder() / (
        "{}_until_{}.{}".format(
            fileUtil.asString(time[0]),
            fileUtil.asString(time[-1]),
            imgFormat
        )
    )
    fig.savefig(image, format=imgFormat, dpi=400)
    # plt.show()

    return image


if __name__ == '__main__':
    plotMeetings()
